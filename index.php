<?php

require_once ('animal.php');
require_once ('ape.php');
require_once ('frog.php');

$sheep = new animal("shaun");

echo "Nama Hewan : " .$sheep->name ."<br>"; // "shaun"
echo "Jumlah Kaki : " .$sheep->legs ."<br>"; // 4
echo "Cold Blooded : " .$sheep->cold_blooded ."<br><br>"; // "no"

$ape = new ape("kera sakti");

echo "Nama Hewan : " .$ape->name ."<br>"; // "shaun"
echo "Jumlah Kaki : " .$ape->legs ."<br>"; // 4
echo "Cold Blooded : " .$ape->cold_blooded ."<br>"; // "no"
echo $ape->yell(). "<br><br>";

$frog = new frog("kodok");

echo "Nama Hewan : " .$frog->name ."<br>"; // "shaun"
echo "Jumlah Kaki : " .$frog->legs ."<br>"; // 4
echo "Cold Blooded : " .$frog->cold_blooded ."<br>"; // "no"
echo $frog->jump();



?>